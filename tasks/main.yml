---
# tasks file for goobi-workflow
- name: "add authorized keys for ubuntu user"
  authorized_key:
    user: ubuntu
    state: present
    key: "{{ item }}"
  loop: "{{ goobi_workflow_ssh_public_keys }}"

- name: Set some pre-configuration by here document.
  debconf:
    name: "{{ item.name }}"
    question: "{{ item.question }}"
    vtype: "{{ item.vtype }}"
    value: "{{ item.value }}"
  with_items: "{{ debconf_vars }}"
  become: true
  environment: "{{ goobi_workflow_environment }}"

- name: Show and check pre-configuration after using debconf.
  command: debconf-show slapd
  become: true

- name: "install dependencies"
  apt:
    name: "{{ goobi_workflow_dependencies }}"
    update_cache: "yes"
  become: true

- name: Reconfigure for the debconf after installation of dependencies.
  command: dpkg-reconfigure -f noninteractive "{{ item }}"
  loop: ["postfix", "slapd", "nslcd", "libnss-ldapd"]
  become: true

- name: "Stop tomcat9"
  systemd:
    name: tomcat9
    enabled: no
    state: stopped
  become: true

- name: Touch a file for setting aliases for tomcat9.
  file:
    path: "/root/.bash_aliases"
    state: touch
    mode: '0644'
  become: true

- name: Set aliases for tomcat9.
  lineinfile:
    path: "/root/.bash_aliases"
    state: present
    regexp: "^{{ item.key }}="
    line: "{{ item.key }}={{ item.value }}"
  with_items: "{{ bash_aliases }}"
  become: true

- name: Source bash_aliases.
  shell: . "/root/.bash_aliases"
  become: true

  #- name: Correct Java version selected.
  #  alternatives:
  #    name: java
  #    path: /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
  #  become: true
  #
  #- name: Set the selected Java for tomcat9.
  #  lineinfile:
  #    path: "/etc/default/tomcat9"
  #    state: present
  #    regexp: "^#JAVA_HOME=.*"
  #    line: "JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/"
  #    backrefs: true
  #  become: true
  #
  #- name: Patch Java accessibility properties.
  #  lineinfile:
  #    path: "/etc/java-8-openjdk/accessibility.properties"
  #    state: present
  #    regexp: '^(assistive_technologies=.*)'
  #    line: '#\1'
  #    backrefs: true
  #  become: true

- name: "create a temporary directory for installation"
  file:
    state: "directory"
    path: "{{ goobi_workflow_install_dir }}/goobi-workflow"
  become: true

- name: "clone goobi worfklow repo"
  git:
    repo: "https://github.com/intranda/goobi-workflow.git"
    dest: "{{ goobi_workflow_install_dir }}/goobi-workflow"
    version: "master"
    #depth: 1
  become: true

- name: Set SLAPD_SERVICES in the file "/etc/default/slapd".
  lineinfile:
    path: "/etc/default/slapd"
    state: present
    regexp: '^(SLAPD_SERVICES=).*'
    line: '\1"ldap://127.0.0.1:389/ ldapi:///"'
    backrefs: true
  become: true

- name: Check if the file "/etc/ldap/schema/samba.schema" is empty.
  stat:
    path: /etc/ldap/schema/samba.schema
  register: samba_status

- name: Print the status of samba.schema file.
  debug:
    var: samba_status.stat

- name: Copy "samba.schema".
  shell: "cat /usr/share/doc/samba/examples/LDAP/samba.schema >> /etc/ldap/schema/samba.schema"
  when: samba_status.stat.exists and samba_status.stat.size < 2
  become: true

- name: Copy "samba.schema".
  shell: "cat /usr/share/doc/samba/examples/LDAP/samba.schema > /etc/ldap/schema/samba.schema"
  when: not samba_status.stat.exists
  become: true

- name: Copy the file "samba.ldif" from goobi installation directory to "/etc/ldap/schema/".
  copy:
    src: "{{goobi_workflow_install_dir}}/goobi-workflow/Goobi/install/ldap/samba.ldif"
    dest: /etc/ldap/schema/
    remote_src: true
  become: true

- name: Insert the schema into the LDAP, this task only need once.
  command: ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/samba.ldif
  become: true
  ignore_errors: true

- name: Copy the file "import.ldif" from goobi installation directory to tmp.
  copy:
    src: "{{goobi_workflow_install_dir}}/goobi-workflow/Goobi/install/ldap/import.ldif"
    dest: "{{ goobi_workflow_install_dir }}/"
    remote_src: true
  become: true

- name: Set BASENAME for in import.ldif.
  replace:
    path: "{{ goobi_workflow_install_dir }}/import.ldif"
    regexp: "(.*)dc=GOOBI,dc=EXAMPLE,dc=ORG"
    replace: '\1{{ ansible_env.BASENAME }}'
  become: true

- name: Print the value of BASENAME.
  debug:
    msg: "the base name is {{ ansible_env.BASENAME }}"

- name: Print the value of PW_LDAP_GOOBI.
  debug:
    msg: "the password: {{ ansible_env.PW_LDAP_GOOBI }}"

- name: Check domain and password for slapd
  shell: |
    slapcat -n0 | grep olcRootDN
    slapcat -n0 | grep olcRootPW
  become: true

- name: Test debconf and slapd setup.
  command: ldapsearch -x -D "cn=admin,{{ ansible_env.BASENAME }}" -w "{{ ansible_env.PW_LDAP_GOOBI }}"
  ignore_errors: true

- name: Delete OUs and the cn=NextFreeUnixId for Goobi.
  shell: |
    ldapdelete -v "cn=admin,{{ ansible_env.BASENAME }}" -D "cn=admin,{{ ansible_env.BASENAME }}" -w "{{ ansible_env.PW_LDAP_GOOBI }}"
  become: true
  ignore_errors: true

- name: Create OUs and the cn=NextFreeUnixId for Goobi.
  shell: |
    ldapadd -x -D "cn=admin,{{ ansible_env.BASENAME }}" -w "{{ ansible_env.PW_LDAP_GOOBI }}" -f "{{ goobi_workflow_install_dir }}/import.ldif"
  become: true

- name: Check if the file /etc/ldap/ldap.conf exists.
  stat:
    path: /etc/ldap/ldap.conf
  register: ldap_conf_status
  become: true

- name: Modify /etc/ldap/ldap.conf if it exists.
  lineinfile:
    path: /etc/ldap/ldap.conf
    line: "{{ item }}"
    state: present
    backup: true
  with_items:
    - "BASE     {{ ansible_env.BASENAME }}"
    - "URI      ldap://127.0.0.1"
    - "TLS_REQCERT  never"
  when: ldap_conf_status.stat.exists
  become: true

- name: Create /etc/ldap/ldap.conf if it does not exists.
  file:
    path: /etc/ldap/ldap.conf
    state: touch
  when: not ldap_conf_status.stat.exists
  become: true

- name: Check if the new created file /etc/ldap/ldap.conf is empty.
  stat:
    path: /etc/ldap/ldap.conf
  register: new_ldap_conf_status
  become: true

- name: Add content to new created /etc/ldap/ldap.conf.
  blockinfile:
    path: /etc/ldap/ldap.conf
    block: |
        #
        # LDAP Defaults
        #
        BASE         {{ ansible_env.BASENAME }}
        URI          ldap://127.0.0.1
        TLS_REQCERT  never
  when: new_ldap_conf_status.stat.exists and new_ldap_conf_status.stat.size < 2
  become: true

- name: Slapd adds Goobi/install/ldap/index.ldif.
  command: ldapmodify -Y EXTERNAL -H ldapi:/// -f "{{ goobi_workflow_install_dir }}/goobi-workflow/Goobi/install/ldap/index.ldif"
  become: true
  ignore_errors: true

- name: Modify /etc/ldapvi.conf.
  lineinfile:
    path: /etc/ldapvi.conf
    line: "{{ item }}"
    state: present
    backup: true
  with_items:
    - "user: cn=admin,{{ ansible_env.BASENAME }}"
    - "password: {{ ansible_env.PW_LDAP_GOOBI }}"
  become: true

- name: chmod 600 /etc/ldapvi.conf.
  file:
    path: /etc/ldapvi.conf
    mode: '0600'
  become: true

- name: Copy the file "smb.conf" from goobi installation directory to /etc/samba/.
  copy:
    src: "{{goobi_workflow_install_dir}}/goobi-workflow/Goobi/install/samba/smb.conf"
    dest: /etc/samba/
    remote_src: true
  become: true

- name: Set BASENAME in smb.conf.
  replace:
    path: /etc/samba/smb.conf
    regexp: "(.*)dc=GOOBI,dc=EXAMPLE,dc=ORG"
    replace: '\1{{ ansible_env.BASENAME }}'
  become: true

- name: Setup password for Samba.
  command: "smbpasswd -w {{ ansible_env.PW_LDAP_GOOBI }}"
  become: true

- name: Restart Samba.
  systemd:
    name: smbd
    state: restarted
  become: true

- name: Copy the file "samba-dfree" from goobi installation directory to /usr/local/bin/.
  copy:
    src: "{{goobi_workflow_install_dir}}/goobi-workflow/Goobi/install/samba/samba-dfree"
    dest: /usr/local/bin/
    remote_src: true
  become: true

- name: Chmod 755 /usr/local/bin/samba-dfree.
  file:
    path: /usr/local/bin/samba-dfree
    mode: '0755'
  become: true

- name: Show existing database
  shell: |
    mysql -u goobi -e "SHOW DATABASES" --password={{ansible_env.PW_LDAP_GOOBI}};
  ignore_errors: true

- name: Setup the database MySQL.
  shell: |
    mysql -e "CREATE DATABASE IF NOT EXISTS goobi;
    USE goobi;
    SOURCE {{goobi_workflow_install_dir}}/goobi-workflow/Goobi/install/db/goobi_blank.sql;
    CREATE USER IF NOT EXISTS 'goobi'@'localhost' IDENTIFIED BY '{{ ansible_env.PW_LDAP_GOOBI }}';
    GRANT ALL PRIVILEGES ON goobi.* TO 'goobi'@'localhost' WITH GRANT OPTION;
    FLUSH PRIVILEGES;"
  become: true

- name: Patch /etc/default/tomcat9 - comment JAVA_OPTS.
  replace:
    path: "/etc/default/tomcat9"
    regexp: '^(JAVA_OPTS="-Djava.awt.headless=true")$'
    replace: '#\1'
  become: true

- name: Patch /etc/default/tomcat9 - add JAVA_OPTS.
  lineinfile:
    path: "/etc/default/tomcat9"
    state: present
    insertafter: 'JAVA_OPTS=.*'
    line: "{{ item }}"
    backup: true
  with_items: "{{ java_opts_vars }}"
  become: true

- name: Patch /etc/default/tomcat9 - add UMASK=0022.
  lineinfile:
    path: "/etc/default/tomcat9"
    state: present
    insertafter: 'JAVA_OPTS=.*'
    line: 'export UMASK=0022'
  become: true

- name: Patch /etc/default/tomcat9 - set LOGFILE_COMPRESS.
  replace:
    path: "/etc/default/tomcat9"
    regexp: '^#(LOGFILE_COMPRESS=.*)'
    replace: '\1'
  become: true

- name: Create a patch for /etc/tomcat9/server.xml in "{{ goobi_workflow_install_dir }}".
  blockinfile:
    path: "{{ goobi_workflow_install_dir }}/tomcat9-server.xml.patch"
    create: true
    block: "{{ patch_tomcat9_server }}"
  become: true

- name: Replace GOOBI_HOSTNAME in patch.
  replace:
    path: "{{ goobi_workflow_install_dir }}/tomcat9-server.xml.patch"
    regexp: '(.*)"GOOBI_HOSTNAME"'
    replace: '\1"{{ ansible_env.NAME_HOST }}"'
  become: true

- name: Patch /etc/tomcat9/server.xml.
  patch:
    src: "{{ goobi_workflow_install_dir }}/tomcat9-server.xml.patch"
    dest: /etc/tomcat9/server.xml
    ignore_whitespace: true
    remote_src: true
  become: true

- name: Create a patch for /etc/tomcat9/context.xml in "{{ goobi_workflow_install_dir }}".
  blockinfile:
    path: "{{ goobi_workflow_install_dir }}/tomcat9-context.xml.patch"
    create: true
    block: "{{ patch_tomcat9_context }}"
  become: true

- name: Patch /etc/tomcat9/context.xml.
  patch:
    src: "{{ goobi_workflow_install_dir }}/tomcat9-context.xml.patch"
    dest: /etc/tomcat9/context.xml
    ignore_whitespace: true
    remote_src: true
  become: true

- name: Make sure the systemd tomcat9 service to have more rights.
  blockinfile:
    path: /etc/systemd/system/tomcat9.service.d/override.conf
    create: true
    block: |
      [Service]
      LogsDirectoryMode=755
      CacheDirectoryMode=755
      ProtectSystem=true
      NoNewPrivileges=false
      ReadWritePaths=/opt/digiverso/
  become: true

- name: Configuration of Apache - enable mod headers module.
  apache2_module:
    state: present
    name: "{{ item }}"
  loop: ["proxy_ajp", "rewrite", "headers"]
  become: true

- name: "create {{ ansible_env.NAME_HOST }}.conf in /etc/apache2/sites-available/"
  blockinfile:
    path: "/etc/apache2/sites-available/{{ ansible_env.NAME_HOST }}.conf"
    create: true
    block: "{{ apache2_sites_available_localhost_conf }}"
  become: true

- name: "Replace GOOBI.EXAMPLE.ORG to {{ ansible_env.NAME_HOST }} in {{ ansible_env.NAME_HOST }}.conf."
  replace:
    path: "/etc/apache2/sites-available/{{ ansible_env.NAME_HOST }}.conf"
    regexp: 'GOOBI.EXAMPLE.ORG'
    replace: '{{ ansible_env.NAME_HOST }}'
  become: true

- name: a2dissite and a2ensite domain.
  shell: |
    a2dissite 000-default.conf
    a2ensite "{{ ansible_env.NAME_HOST }}.conf"
  become: true

- name: Restart apache2.
  systemd:
    name: apache2
    state: restarted
  become: true

- name: Create goobi sudoers file.
  blockinfile:
    path: "{{ goobi_workflow_install_dir }}/70_goobi"
    create: true
    block: "{{ goobi_sudoers_file }}"
  become: true

- name: visudo goobi sudoers file.
  shell: cat "{{ goobi_workflow_install_dir }}/70_goobi" | EDITOR='tee' visudo -f /etc/sudoers.d/70_goobi
  become: true

- name: Create goobi directories.
  file:
    path: "/opt/digiverso/{{ item }}"
    state: directory
    mode: '0755'
  loop: "{{ goobi_folders }}"
  become: true

- name: Copy files from goobi installation repo directory to created goobi directories
  copy:
    src: "{{goobi_workflow_install_dir}}/goobi-workflow/Goobi/install/{{ item }}"
    dest: "/opt/digiverso/goobi/"
    remote_src: true
  loop: "{{ goobi_created_dirs }}"
  become: true

- name: Set the owner tomcat9 for /opt/digiverso/.
  file:
    path: /opt/digiverso/
    state: directory
    recurse: true
    owner: tomcat
  become: true

- name: Download Java Advanced Imaging (JAI).
  get_url:
    url: "{{ item }}"
    dest: "{{ goobi_workflow_install_dir }}"
  loop: "{{ java_JAI_urls }}"
  become: true

- name: Unarchive downloaded Java Advanced Imaging (JAI).
  unarchive:
    src: "{{ goobi_workflow_install_dir }}/{{ item }}"
    dest: "{{ goobi_workflow_install_dir }}"
    remote_src: true
  loop: "{{ java_JAI_jar_zips }}"
  become: true

- name: Copy downloaded Java Advanced Imaging (JAI).
  copy:
    src: "{{ goobi_workflow_install_dir }}/{{ item }}"
    dest: /opt/digiverso/tomcat-lib/
    remote_src: true
  loop: "{{ java_JAI_jars }}"
  become: true


- name: Delete goobi.xml if it exists
  file:
    path: "/etc/tomcat9/Catalina/localhost/goobi.xml"
    state: absent
  become: true

- name: "Create goobi.xml"
  lineinfile:
    path: "/etc/tomcat9/Catalina/localhost/goobi.xml"
    create: true
    line: "<?xml version='1.0' encoding='utf-8'?>"
  become: true

- name: "Append goobi.xml."
  blockinfile:
    path: "/etc/tomcat9/Catalina/localhost/goobi.xml"
    marker: "<!-- {mark} ANSIBLE MANAGED BLOCK -->"
    insertafter: "<?xml version='1.0' encoding='utf-8'?>"
    block: "{{ tomcat9_goobi_xml }}"
  become: true

- name: "Replace PW_SQL_GOOBI in goobi.xml."
  replace:
    path: "/etc/tomcat9/Catalina/localhost/goobi.xml"
    regexp: 'PW_SQL_GOOBI'
    replace: '{{ ansible_env.PW_SQL_GOOBI }}'
  become: true

- name: Adjust the file permissions for /etc/tomcat9/Catalina.
  file:
    path: /etc/tomcat9/Catalina
    state: directory
    recurse: true
    owner: root
    group: tomcat
    mode: g-w
  become: true

- name: Download goobi.war.
  get_url:
    url: https://github.com/intranda/goobi-workflow/releases/latest/download/goobi.war
    dest: /var/lib/tomcat9/webapps/
  become: true

- name: Deployment of Plugins - clone plugins.
  git:
    repo: "{{ item.repo }}"
    dest: "{{ goobi_workflow_install_dir }}/{{ item.dir }}"
    version: "master"
    #depth: 1
  loop: "{{ goobi_plugins_git_clones }}"
  register: source_code_clones
  become: true

- name: Execute mvn package for each plugin.
  command: mvn package
  args:
    chdir: "{{ goobi_workflow_install_dir }}/{{ item }}"
  loop: "{{ goobi_plugin_packages }}"
  become: true

- name: Copy plagins.
  copy:
    src: "{{ goobi_workflow_install_dir }}/{{ item.src }}"
    dest: "/opt/digiverso/goobi/{{ item.dest }}"
    remote_src: true
  loop: "{{ goobi_copy_plugins_vars }}"
  become: true

- name: "Replace content in plugin_intranda_dashboard_extended.xml."
  replace:
    path: "/opt/digiverso/goobi/config/plugin_intranda_dashboard_extended.xml"
    regexp: '<html-box-show>true<\/html-box-show>'
    replace: '<html-box-show>false</html-box-show>'
  become: true

- name: Restart the tomcat service.
  systemd:
    name: tomcat9
    enabled: true
    state: started
    daemon_reload: true
  become: true

- name: Get SID value.
  shell: net getlocalsid | awk '{print $NF}'
  register: SID_value
  become: true

- debug:
    var: SID_value.stdout

- name: Set SID value.
  shell: SID="{{ SID_value.stdout }}"
  become: true

- name: Get NLTMHASH_PW_GOOBITESTUSER.
  shell: python3 -c "import hashlib; pw = '{{ ansible_env.PW_GOOBITESTUSER }}'; print(hashlib.new('md4', pw.encode('utf-16le')).hexdigest().upper());"
  register: NLTMHASH_PW_GOOBITESTUSER_value
  become: true

- debug:
    var: NLTMHASH_PW_GOOBITESTUSER_value.stdout

- name: Set NLTMHASH_PW_GOOBITESTUSER value.
  shell: NLTMHASH_PW_GOOBITESTUSER="{{ NLTMHASH_PW_GOOBITESTUSER_value.stdout }}"
  become: true

- name: Get SLAPHASH_PW_GOOBITESTUSER value.
  shell: slappasswd -s "{{ ansible_env.PW_GOOBITESTUSER }}"
  register: SLAPHASH_PW_GOOBITESTUSER_value
  become: true

- debug:
    var: SLAPHASH_PW_GOOBITESTUSER_value.stdout

- name: Set SLAPHASH_PW_GOOBITESTUSER value.
  shell: SLAPHASH_PW_GOOBITESTUSER="{{ SLAPHASH_PW_GOOBITESTUSER_value.stdout }}"
  become: true

- name: Copy testuser.ldif from goobi repo to "{{ goobi_workflow_install_dir }}".
  copy:
    src: "{{ goobi_workflow_install_dir }}/goobi-workflow/Goobi/install/ldap/testuser.ldif"
    dest: "{{ goobi_workflow_install_dir }}/"
    remote_src: true
  become: true

- name: Replace contents in copied testuser.ldif.
  replace:
    path: "{{ goobi_workflow_install_dir }}/testuser.ldif"
    regexp: "{{ item.regexp }}"
    replace: "{{ item.replacement }}"
  with_items:
    - regexp: 'S-1-5-21-531335965-4077168823-1713822973'
      replacement: "{{ SID_value.stdout }}"
    - regexp: 'dc=GOOBI,dc=EXAMPLE,dc=ORG'
      replacement: "{{ ansible_env.BASENAME }}"
    - regexp: 'sambaNTPassword: 0CB6948805F797BF2A82807973B89537'
      replacement: "sambaNTPassword: {{ NLTMHASH_PW_GOOBITESTUSER_value.stdout }}"
    - regexp: 'userPassword:: e1NTSEF9ZWtKaFpTZklJcndHdjlKTU1rYmxmOUwvZGQ3S3pmU1Y='
      replacement: "userPassword: {{ SLAPHASH_PW_GOOBITESTUSER_value.stdout }}"
  become: true

- name: Add the test accounts to the LDAP.
  shell: ldapadd -x -D "cn=admin,{{ ansible_env.BASENAME }}" -w "{{ ansible_env.PW_LDAP_GOOBI }}" -f "{{ goobi_workflow_install_dir }}/testuser.ldif"
  become: true

- name: Restart the nscd service.
  systemd:
    name: nscd
    enabled: true
    state: restarted
  become: true

- name: Pause for 30 seconds.
  pause:
    seconds: 30

- name: Ensure the users are known to the system.
  shell: id testadmin
  become: true

- name: Create directories for the user testadmin.
  file:
    path: "/home/{{ item }}"
    state: directory
    owner: "{{ item }}"
    group: tomcat
    mode: '0775'
  loop: "{{ goobi_user_testadmin_dirs }}"
  become: true

- name: Use the just installed local LDAP.
  shell: |
    mysql goobi -e "update ldapgruppen
        set
          sambaSID='$SID-{uidnumber*2+1000}',
          sambaPrimaryGroupSID='$SID-100',
          userDN='cn={login},ou=users,ou=goobi,{{ansible_env.BASENAME}}',
          adminLogin='cn=admin,{{ansible_env.BASENAME}}',
          adminPassword='{{ansible_env.PW_LDAP_GOOBI}}',
          ldapUrl='ldap://localhost:389/',
          nextFreeUnixId='cn=NextFreeUnixId,{{ansible_env.BASENAME}}',
          encryptionType='SHA',
          useSsl=0,
          authenticationType='ldap',
          readonly=0,
          readDirectoryAnonymous=0,
          useLocalDirectoryConfiguration=0,
          ldapHomeDirectoryAttributeName='homeDirectory',
          useTLS=0
        where ldapgruppenID=2;"
  become: true

- name: Remove an unused user.
  shell: mysql goobi -e "delete from benutzer where login='goobi';"
  become: true

- name: A test of the SMB access.
  shell: smbclient -U testadmin%$PW_GOOBITESTUSER //localhost/testadmin -c dir
  become: true
