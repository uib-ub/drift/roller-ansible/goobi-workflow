Role Name
=========

The goobi-workflow is a Ansible role which can be used to automatically set up a Goobi workflow on a server.

Requirements
------------

The goobi-workflow role currently works by setting up Goobi workflow on an existing Ubuntu 20.04 server (eg. on NREC servers), which means the Ubuntu setup is required before using the goobi-workflow role. See Ansible role [Ubuntu](https://git.app.uib.no/uib-ub/drift/roller-ansible/ubuntu) for setting up Ubuntu. 

Role Variables
--------------

Most of variables for Goobi workflow setup are defined in defaults/main, however, the environment variables are defined in vars/main. For other needed variables, please check the usage in [ansible-goobi](https://git.app.uib.no/samla/ansible-goobi) repo.

Dependencies
------------

Ansible role [Ubuntu](https://git.app.uib.no/uib-ub/drift/roller-ansible/ubuntu).

Example Playbook
----------------

  - name: "import role goobi-workflow"
    import_role:
      name: "goobi-workflow"

See [goobi-workflow playbook](https://git.app.uib.no/samla/ansible-goobi/-/blob/master/goobi-workflow.yml).

License
-------

BSD

Author Information
------------------
Rui Wang, University of Bergen
