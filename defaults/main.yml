---
# defaults file for goobi-workflow
goobi_workflow_dependencies: ["openjdk-11-jdk-headless", "tomcat9", "mariadb-server", "apache2", "samba", "smbclient", "imagemagick", "graphicsmagick", "libtiff-tools", "djvulibre-bin", "netpbm", "jhead", "exiv2", "bc", "unzip", "maven", "git", "mailutils", "slapd", "libnss-ldapd", "libldap-2.4-2", "ldap-utils", "ldapvi", "libpam-ldapd", "smbldap-tools", "rename"]

goobi_workflow_host: localhost
goobi_workflow_basename: dc=localhost

tomcat_version: tomcat9
tomcat9_service_dir: /etc/systemd/system/multi-user.target.wants/

goobi_workflow_env_pw: !vault |
    $ANSIBLE_VAULT;1.1;AES256
    64353962643830356366656635383930666235346161373437373366623930353363356330613432
    3465313366656532393937633937343434343063303838640a346263336263653039386334376238
    34326336653330386133303336353038626465303738633164663036636633326133386533623933
    3937333334663465360a313532343963373163656634613630303738623363336130306466373432
    36636130303839613764633432383330343637346436626364353235346565636239616533316661
    3831366366393063636233613836393536393734366631666533

goobi_workflow_install_dir: /tmp/install

bash_aliases:
    - key: alias cata
      value: "'journalctl -n200 -f -u {{ tomcat_version }}'"
    - key: alias ct
      value: "'chown tomcat: *'"
    - key: alias ctr
      value: "'chown -R tomcat: *'"

debconf_vars:
    - name: postfix
      question: postfix/main_mailer_type
      vtype: select
      value: Local only
    - name: postfix
      question: postfix/mailname
      vtype: string
      value: "{{ ansible_env.NAME_HOST }}"
    - name: slapd
      question: slapd/internal/adminpw
      vtype: password
      value: "{{ ansible_env.PW_LDAP_GOOBI }}"
    - name: slapd
      question: slapd/password1
      vtype: password
      value: "{{ ansible_env.PW_LDAP_GOOBI }}"
    - name: slapd
      question: slapd/password2
      vtype: password
      value: "{{ ansible_env.PW_LDAP_GOOBI }}"
    - name: slapd
      question: shared/organization
      vtype: string
      value: "{{ ansible_env.NAME_HOST }}"
    - name: slapd
      question: slapd/domain
      vtype: string
      value: "{{ ansible_env.NAME_HOST }}"
    - name: slapd
      question: slapd/backend
      vtype: select
      value: MDB
    - name: nslcd
      question: nslcd/ldap-uris
      vtype: string
      value: ldap://127.0.0.1/
    - name: nslcd
      question: nslcd/ldap-base
      vtype: string
      value: "{{ ansible_env.BASENAME }}"
    - name: libnss-ldapd
      question: libnss-ldapd/nsswitch
      vtype: multiselect
      value: passwd, group, shadow

java_opts_vars:
    - JAVA_OPTS="-Djava.awt.headless=true -Xmx2g -Xms2g"
    - JAVA_OPTS="${JAVA_OPTS} -XX:+UseG1GC"
    - JAVA_OPTS="${JAVA_OPTS} -XX:+ParallelRefProcEnabled"
    - JAVA_OPTS="${JAVA_OPTS} -XX:+DisableExplicitGC"
    - JAVA_OPTS="${JAVA_OPTS} -XX:+CMSClassUnloadingEnabled"
    - JAVA_OPTS="${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom"
    - JAVA_OPTS="${JAVA_OPTS} -Dfile.encoding='utf-8'"

goobi_folders: ["logs", "tomcat-lib", "viewer/hotfolder", "goobi/activemq", "goobi/config", "goobi/lib", "goobi/metadata", "goobi/rulesets", "goobi/scripts", "goobi/static_assets", "goobi/tmp", "goobi/xslt", "goobi/plugins/administration", "goobi/plugins/command", "goobi/plugins/dashboard", "goobi/plugins/export", "goobi/plugins/GUI", "goobi/plugins/import", "goobi/plugins/opac", "goobi/plugins/statistics", "goobi/plugins/step", "goobi/plugins/validation", "goobi/plugins/workflow"]

goobi_created_dirs: ["config", "scripts", "rulesets", "xslt"]

goobi_user_testadmin_dirs: ["testadmin", "testscanning", "testmetadata", "testimaging", "testqc", "testprojectmanagement"]

patch_tomcat9_server: |
    @@ -66,59 +66,19 @@
              APR (HTTP/AJP) Connector: /docs/apr.html
              Define a non-SSL/TLS HTTP/1.1 Connector on port 8080
         -->
    -    <Connector port="8080" protocol="HTTP/1.1"
    +    <Connector address="127.0.0.1" port="8080" protocol="HTTP/1.1"
                    connectionTimeout="20000"
    -               redirectPort="8443" />
    -    <!-- A "Connector" using the shared thread pool-->
    -    <!--
    -    <Connector executor="tomcatThreadPool"
    -               port="8080" protocol="HTTP/1.1"
    -               connectionTimeout="20000"
    -               redirectPort="8443" />
    -    -->
    -    <!-- Define an SSL/TLS HTTP/1.1 Connector on port 8443
    -         This connector uses the NIO implementation. The default
    -         SSLImplementation will depend on the presence of the APR/native
    -         library and the useOpenSSL attribute of the
    -         AprLifecycleListener.
    -         Either JSSE or OpenSSL style configuration may be used regardless of
    -         the SSLImplementation selected. JSSE style configuration is used below.
    -    -->
    -    <!--
    -    <Connector port="8443" protocol="org.apache.coyote.http11.Http11NioProtocol"
    -               maxThreads="150" SSLEnabled="true">
    -        <SSLHostConfig>
    -            <Certificate certificateKeystoreFile="conf/localhost-rsa.jks"
    -                         type="RSA" />
    -        </SSLHostConfig>
    -    </Connector>
    -    -->
    -    <!-- Define an SSL/TLS HTTP/1.1 Connector on port 8443 with HTTP/2
    -         This connector uses the APR/native implementation which always uses
    -         OpenSSL for TLS.
    -         Either JSSE or OpenSSL style configuration may be used. OpenSSL style
    -         configuration is used below.
    -    -->
    -    <!--
    -    <Connector port="8443" protocol="org.apache.coyote.http11.Http11AprProtocol"
    -               maxThreads="150" SSLEnabled="true" >
    -        <UpgradeProtocol className="org.apache.coyote.http2.Http2Protocol" />
    -        <SSLHostConfig>
    -            <Certificate certificateKeyFile="conf/localhost-rsa-key.pem"
    -                         certificateFile="conf/localhost-rsa-cert.pem"
    -                         certificateChainFile="conf/localhost-rsa-chain.pem"
    -                         type="RSA" />
    -        </SSLHostConfig>
    -    </Connector>
    -    -->
    -
    -    <!-- Define an AJP 1.3 Connector on port 8009 -->
    -    <!--
    -    <Connector protocol="AJP/1.3"
    -               address="::1"
    -               port="8009"
    -               redirectPort="8443" />
    -    -->
    +              maxThreads="400"
    +              URIEncoding="UTF-8"
    +              enableLookups="false"
    +              disableUploadTimeout="true"
    +              proxyName="localhost"
    +              proxyPort="80" />
    +
    +    <Connector address="127.0.0.1" port="8009" protocol="AJP/1.3"
    +              connectionTimeout="20000"
    +              maxThreads="400"
    +              URIEncoding="UTF-8" secretRequired="true" secret="test" />

         <!-- An Engine represents the entry point (within Catalina) that processes
              every request.  The Engine implementation for Tomcat stand alone
    @@ -161,9 +121,9 @@
             <!-- Access log processes all example.
                  Documentation at: /docs/config/valve.html
                  Note: The pattern used is equivalent to using pattern="common" -->
    -        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
    +        <!--Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
                    prefix="localhost_access_log" suffix=".txt"
    -               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
    +               pattern="%h %l %u %t &quot;%r&quot; %s %b" /-->

           </Host>
         </Engine>

patch_tomcat9_context: |
    @@ -24,7 +24,5 @@
         <WatchedResource>${catalina.base}/conf/web.xml</WatchedResource>

         <!-- Uncomment this to disable session persistence across Tomcat restarts -->
    -    <!--
         <Manager pathname="" />
    -    -->
     </Context>

apache2_sites_available_localhost_conf: |
        <VirtualHost *:80>
            ServerAdmin webmaster@samla.no
            ServerName goobi.samla.no
            DocumentRoot /var/www/html/

            RequestHeader unset Expect early

             ## compress output
             <IfModule mod_deflate.c>
                      AddOutputFilterByType DEFLATE text/plain text/html text/xml
                      AddOutputFilterByType DEFLATE text/css text/javascript
                      AddOutputFilterByType DEFLATE application/xml application/xhtml+xml
                      AddOutputFilterByType DEFLATE application/rss+xml
                      AddOutputFilterByType DEFLATE application/javascript application/x-javascript
             </IfModule>

             ## general proxy settings
             ProxyPreserveHost On
             ProxyVia On
             SetEnv force-proxy-request-1.0 1
             SetEnv proxy-nokeepalive 1
             <Proxy *>
                      Require local
             </Proxy>

             ## goobi
             redirect /index.html http://goobi.samla.no/goobi/uii
             redirectmatch /goobi$ http://goobi.samla.no/goobi/uii

             <Location "/goobi/">
                      Require all granted
                      ProxyPass ajp://localhost:8009/goobi/ secret=test
                      ProxyPassReverse ajp://localhost:8009/goobi/
             </Location>

             ## disable external connection to Goobi WebAPI globally
             # ProxyPass /goobi/wi !

             ## only allow access to Goobi WebAPI for certain hosts
             <Location "/goobi/wi">
                      # Require ip 1.2.3.4
                      Require local
             </Location>

             ## logging
             CustomLog /var/log/apache2/GOOBI.EXAMPLE.ORG_access.log combined
             ErrorLog /var/log/apache2/GOOBI.EXAMPLE.ORG_error.log

             # Possible values include: debug, info, notice, warn, error, crit, alert, emerg.
             LogLevel warn
        </VirtualHost>

goobi_sudoers_file: |
    ##
    # Goobi sudoers file
    ##

    # Berechtigungen fuer Tomcat Server
    User_Alias HTTPD=tomcat


    # In den Shellscripten definierte Kommandos:
    # script_chownTomcat.sh, script_chownUser.sh, script_createDirUserHome.sh,
    # script_createSymLink.sh
    Cmnd_Alias CHOWN_DATEN=/bin/chown * /opt/digiverso/goobi/metadata/*

    # script_createDirUserHome.sh
    Cmnd_Alias CHGRP_USERS=/bin/chgrp tomcat /home/*
    Cmnd_Alias CHMOD_USERS=/bin/chmod g+w /home/*
    Cmnd_Alias CHMOD_WRITE=/bin/chmod * /opt/digiverso/goobi/metadata/*
    Cmnd_Alias CHOWN_USERS=/bin/chown * /home/*
    Cmnd_Alias MKDIR_USERS=/bin/mkdir /home/*

    # Der Tomcat Server darf die in den Shellscripten definierten Kommandos ohne
    # Passwortabfrage mit root Berechtigung ausfuehren
    HTTPD ALL=NOPASSWD: CHOWN_DATEN, CHGRP_USERS, CHMOD_USERS, CHOWN_USERS, MKDIR_USERS, CHMOD_WRITE

    HTTPD ALL=NOPASSWD: /bin/mount --bind /opt/digiverso/goobi/metadata/* /home/*
    HTTPD ALL=NOPASSWD: /bin/umount -l /home/*
    HTTPD ALL=NOPASSWD: /bin/rmdir /home/*
    HTTPD ALL=NOPASSWD: /bin/chmod * /home/*

tomcat9_goobi_xml: |
    <Context>
            <Manager className="org.apache.catalina.session.PersistentManager" saveOnRestart="false">
                    <Store className="org.apache.catalina.session.FileStore"/>
            </Manager>

            <Resources>
                    <PreResources
                      className="org.apache.catalina.webresources.DirResourceSet"
                      base="/opt/digiverso/tomcat-lib/"
                      webAppMount="/WEB-INF/lib" />

                    <PreResources
                      className="org.apache.catalina.webresources.DirResourceSet"
                      base="/opt/digiverso/goobi/plugins/GUI/"
                      webAppMount="/WEB-INF/lib" />

                    <PostResources
                      className="org.apache.catalina.webresources.DirResourceSet"
                      base="/opt/digiverso/goobi/lib/"
                      webAppMount="/WEB-INF/lib" />
            </Resources>


    <Resource name="goobi"
              auth="Container"

              factory="org.apache.tomcat.jdbc.pool.DataSourceFactory"
              type="javax.sql.DataSource"

              driverClassName="com.mysql.jdbc.Driver"
              username="goobi"
              password="PW_SQL_GOOBI"
              maxActive="100"
              maxIdle="30"
              minIdle="4"
              maxWait="10000"
              testOnBorrow="true"
              testWhileIdle="true"
              validationQuery="SELECT SQL_NO_CACHE 1"
              removeAbandoned="true"
              removeAbandonedTimeout="600"
              url="jdbc:mysql://localhost/goobi?characterEncoding=UTF-8&amp;autoReconnect=true&amp;autoReconnectForPools=true" />

    </Context>

java_JAI_urls:
    - http://www.java2s.com/Code/JarDownload/jai/jai_codec-1.1.2_01.jar.zip
    - http://www.java2s.com/Code/JarDownload/jai/jai_core-1.1.2_01.jar.zip
    - http://www.java2s.com/Code/JarDownload/jai/jai_imageio-1.1.jar.zip

java_JAI_jar_zips:
    - jai_codec-1.1.2_01.jar.zip
    - jai_core-1.1.2_01.jar.zip
    - jai_imageio-1.1.jar.zip

java_JAI_jars:
    - jai_codec-1.1.2_01.jar
    - jai_core-1.1.2_01.jar
    - jai_imageio-1.1.jar

goobi_plugins_git_clones:
    - repo: https://github.com/intranda/goobi-plugin-opac-pica.git
      dir: goobi-plugin-opac-pica
    - repo: https://github.com/intranda/goobi-plugin-opac-marc.git
      dir: goobi-plugin-opac-marc
    - repo: https://github.com/intranda/goobi-plugin-step-fileupload.git
      dir: goobi-plugin-step-fileupload
    - repo: https://github.com/intranda/goobi-plugin-step-imageqa.git
      dir: goobi-plugin-step-imageqa
      # - repo: https://github.com/intranda/goobi-plugin-command-intranda.git
      #  dir: goobi-plugin-command-intranda
    - repo: https://github.com/intranda/goobi-plugin-dashboard-example.git
      dir: goobi-plugin-dashboard-example
    - repo: https://github.com/intranda/goobi-plugin-rest-intranda.git
      dir: goobi-plugin-rest-intranda
    - repo: https://github.com/intranda/goobi-plugin-controlling-intranda.git
      dir: goobi-plugin-controlling-intranda

goobi_plugin_packages:
    - goobi-plugin-opac-pica/PicaOpacPlugin
    - goobi-plugin-opac-marc/goobi-plugin-opac-marc
    - goobi-plugin-step-fileupload/goobi-plugin-step-fileupload
    - goobi-plugin-step-imageqa/ImageQAPlugin
      # - goobi-plugin-command-intranda/goobi-plugin-command-intranda
    - goobi-plugin-dashboard-example/plugin
    - goobi-plugin-rest-intranda/goobi-plugin-rest-intranda
    - goobi-plugin-controlling-intranda/goobi-plugin-statistics-intranda

goobi_copy_plugins_vars:
    - src: goobi-plugin-opac-pica/PicaOpacPlugin/target/plugin_intranda_opac_pica.jar
      dest: plugins/opac/
    - src: goobi-plugin-opac-marc/goobi-plugin-opac-marc/module-main/target/plugin_intranda_opac_marc.jar
      dest: plugins/opac/
    - src: goobi-plugin-step-fileupload/goobi-plugin-step-fileupload/module-main/target/plugin_intranda_step_fileUpload.jar
      dest: plugins/step/
    - src: goobi-plugin-step-fileupload/goobi-plugin-step-fileupload/plugin_intranda_step_fileUpload.xml
      dest: config/
    - src: goobi-plugin-step-fileupload/goobi-plugin-step-fileupload/module-gui/target/plugin_intranda_step_fileUpload-GUI.jar
      dest: plugins/GUI/
    - src: goobi-plugin-step-imageqa/ImageQAPlugin/plugin_intranda_step_imageQA.xml
      dest: config/
    - src: goobi-plugin-step-imageqa/ImageQAPlugin/module-gui/target/plugin_intranda_step_imageQA-GUI.jar
      dest: plugins/GUI/
    - src: goobi-plugin-step-imageqa/ImageQAPlugin//module-main/target/plugin_intranda_step_imageQA.jar
      dest: plugins/step/
      # - src: goobi-plugin-command-intranda/goobi-plugin-command-intranda/target/plugin_intranda_command_default.jar
      #   dest: plugins/command/
    - src: goobi-plugin-dashboard-example/plugin/module-gui/target/plugin_intranda_dashboard_extended-GUI.jar
      dest: plugins/GUI/
    - src: goobi-plugin-dashboard-example/plugin/module-main/target/plugin_intranda_dashboard_extended.jar
      dest: plugins/dashboard/
    - src: goobi-plugin-dashboard-example/plugin/plugin_intranda_dashboard_extended.xml
      dest: config/
    - src: goobi-plugin-rest-intranda/goobi-plugin-rest-intranda/target/plugin_intranda_rest_default.jar
      dest: lib/
    - src: goobi-plugin-controlling-intranda/goobi-plugin-statistics-intranda/statistics_template.pdf
      dest: plugins/statistics/
    - src: goobi-plugin-controlling-intranda/goobi-plugin-statistics-intranda/statistics_template.xls
      dest: plugins/statistics/
    - src: goobi-plugin-controlling-intranda/goobi-plugin-statistics-intranda/module-gui/target/plugin_intranda_statistics-GUI.jar
      dest: plugins/GUI/
    - src: goobi-plugin-controlling-intranda/goobi-plugin-statistics-intranda/module-main/target/plugin_intranda_statistics.jar
      dest: plugins/statistics/
    - src: goobi-plugin-dashboard-example/plugin/plugin_intranda_dashboard_extended.xml
      dest: config/

